
package Etournament;
/**
 *
 * @authors it21338,it21374
 */
public class Participant {
    
        private String PName;   //The participant's name.
        private String Nickname;    //The participant's nickname.
        private int PType;          //The participant's type(player,administrator)
        private String Communication;   //The participant's communication(email,phonenumber)
        private String Game;            //The game the participant belongs to.

    public Participant(String PName, String Nickname, int PType, String Communication, String Game) {   //Constructor for Participant class.
        this.PName = PName;
        this.Nickname = Nickname;
        this.PType = PType;
        this.Communication = Communication;
        this.Game = Game;
    }

    public String getGame() {   //Returns the participant's game.
        return Game;
    }

    public void setGame(String Game) {  //Sets the participant's game.
        this.Game = Game;
    }


    public String getCommunication() {  //...
        return Communication;
    }

    public void setCommunication(String Communication) {    //...
        this.Communication = Communication;
    }

   
    public String getPName() {      //...
        return PName;
    }

    public void setPName(String PName) {    //...
        this.PName = PName;
    }

    public String getNickname() {       //...
        return Nickname;
    }

    public void setNickname(String Nickname) {      //...
        this.Nickname = Nickname;   
    }

    public int getPType() {         //...
        return PType;
    }

    public void setPType(int PType) {       //...
        this.PType = PType;
    }

} 