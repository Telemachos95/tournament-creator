
package Etournament;
/**
 *
 * @authors it21338,it21374
 */
public class Administrator extends Participant {        //The Administrator class inherits all the fields and methods of class Participant.
    private String PhoneNumber;                         //The admin's phonenumber.

    public Administrator(String PhoneNumber, String PName, String Nickname, int PType, String Communication, String Game) {     //The Admin's class constructor.
        super(PName, Nickname, PType, Communication, Game);
        this.PhoneNumber = PhoneNumber;
    }

   
    public String getPhoneNumber() {    //Returns the admin's phonenumber.
        return PhoneNumber;
    }

    public void setPhoneNumber(String PhoneNumber) {    //Sets the phonenumber.
        this.PhoneNumber = PhoneNumber;
    }
    
    
}
