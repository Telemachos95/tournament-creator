
package Etournament;
/**
 *
 * @authors it21338,it21374
 */
import java.util.ArrayList; //Required in order to create an Arraylist.
import java.util.Scanner;   //Required in order to take input from the keyboard.

public class Options {

private int startYear;      //The year the game will start.(Used for checks).
private int startMonth;     //The month the game will start.(Used for checks).
private int startDay;       //The day the tournament will start.(Used for checks).
private int endYear;        //The year the game will end.(Used for checks).
private int endMonth;       //The month the game will end.(Used for checks).
private int endDay;         //The day the game will end.(Used for checks).
private  int participants;  //The maximum number of players.(Used for checks).
private int checkAdministrator;     //Checks if the game has already an administrator.(Used for checks).
private int howManyPlayers;         //(Used for checks).
private String GameName;        //The name of the game.(Used for checks).
public static int ParticipantCount;     //Every time a new participant is added,it increases by one.(Used for checks).

Tournament myTournament;        //Declares a new Tournament object.
Game myGame;                    //Declares a new Game object.
TimeGame myTimeGame;            //Declares a new TimeGame object.
RoundGame myRoundGame;          //Declares a new RoundGame object.
Participant myParticipant;      //Declares a new Participant object.
Player myPlayer;                //Declares a new Player object.
Administrator myAdmin;          //Declares a new Administrator object.

public Options(){           //Constructor of the Options class/Initialisation.
    ParticipantCount=0;
    GameName="";
    howManyPlayers=0;
    checkAdministrator = 0;
    myGame = new Game();
    myTimeGame = new TimeGame(0,"", "", 0, 0, 0, 0, 0, 0, 0);
    myRoundGame = new RoundGame("","",0,0,0,0,0,0,0);
    myTournament = new Tournament();
    myParticipant = new Participant("","",0,"","");
    myPlayer = new Player("","","",0,"","");
    myAdmin = new Administrator("","","",0,"","");
}

  public void StartTournament() {       //"Starts"(Creates) the tournament.
    
        System.out.println("**Tournament Creation**");
                        
            System.out.println("Give a name for the Tournament:");      
            Scanner input = new Scanner(System.in);
            String name = input.nextLine();
            myTournament.setName(name);
        
            myTournament.getTournamentStartDay();
            System.out.println("Please type the date which the tournament will start:");
            System.out.println("Day (1-31) :");
            startDay = input.nextInt();
            myTournament.setTournamentStartDay(startDay);
            while(myTournament.getTournamentStartDay()<=0 || myTournament.getTournamentStartDay()>31) {     //Checks if the day is between 1-31
                System.out.println("Wrong input,please try again!\n");
                startDay = input.nextInt();
                myTournament.setTournamentStartDay(startDay);                
            }
            
            myTournament.getTournamentStartMonth();
            System.out.println("Month (1-12) :");
            startMonth = input.nextInt();
            myTournament.setTournamentStartMonth(startMonth);            
            while(myTournament.getTournamentStartMonth()<=0 || myTournament.getTournamentStartMonth()>12){      //Checks if the month is between 1-12.
                System.out.println("Wrong input,please try again!\n");
                startMonth = input.nextInt();
                myTournament.setTournamentStartMonth(startMonth);
            }
           
            myTournament.getTournamentStartYear();
            System.out.println("Year (2015-2020) :");
            startYear = input.nextInt();
            myTournament.setTournamentStartYear(startYear);
            while(myTournament.getTournamentStartYear()<=2014 || myTournament.getTournamentStartYear()>2020){       //Checks if the year is between 2015-2020.
                System.out.println("Wrong input,please try again!\n");
                startYear = input.nextInt();
                myTournament.setTournamentStartYear(startYear);
            }
                       
            System.out.println("Please type the date which the tournament will end:");
            
            myTournament.getTournamentEndYear();
            int TournamentEndYear= myTournament.getTournamentStartYear()+2;
            System.out.println("Year ("+myTournament.getTournamentStartYear()+"-"+TournamentEndYear+") :");
            endYear = input.nextInt();
            myTournament.setTournamentEndYear(endYear);
            while(myTournament.getTournamentEndYear()< myTournament.getTournamentStartYear() || myTournament.getTournamentEndYear()> myTournament.getTournamentStartYear()+2){ 
                System.out.println("Wrong input,please try again!\n("+myTournament.getTournamentStartYear()+"-2020)");
                endYear = input.nextInt();
                myTournament.setTournamentEndYear(endYear);
            }
            
            myTournament.getTournamentEndMonth();            
            System.out.println("Month:");
            endMonth = input.nextInt();
            myTournament.setTournamentEndMonth(endMonth);
            if(myTournament.getTournamentEndYear()==myTournament.getTournamentStartYear()){
                while(myTournament.getTournamentEndMonth() < myTournament.getTournamentStartMonth() || myTournament.getTournamentEndMonth() >12) {
                    System.out.println("Wrong input,please try again!\n("+myTournament.getTournamentStartMonth()+"-12)");
                    endMonth = input.nextInt();
                    myTournament.setTournamentEndMonth(endMonth);
                } 
            }else{
                while(myTournament.getTournamentEndMonth() <=0 || myTournament.getTournamentEndMonth() >12) {
                    System.out.println("Wrong input,please try again!\n(1-12)");
                    endMonth = input.nextInt();
                    myTournament.setTournamentEndMonth(endMonth);
                } 
            }
            
            
            myTournament.getTournamentEndDay();
            System.out.println("Day:");
            endDay = input.nextInt();
            myTournament.setTournamentEndDay(endDay);
            if(myTournament.getTournamentEndYear()==myTournament.getTournamentStartYear()) {
                if(myTournament.getTournamentEndMonth()==myTournament.getTournamentStartMonth()){
                    while(myTournament.getTournamentEndDay()< myTournament.getTournamentStartDay() || myTournament.getTournamentEndDay()>32 ){
                        System.out.println("Wrong input,please try again!\n("+myTournament.getTournamentStartDay()+"-31)");
                        endDay = input.nextInt();
                        myTournament.setTournamentEndDay(endDay);
                    }                 
                }
                
            }else{
                while(myTournament.getTournamentEndDay()<=0 || myTournament.getTournamentEndDay()>31 ){
                    System.out.println("Wrong input,please try again!\n(1-31)");
                    endDay = input.nextInt();
                    myTournament.setTournamentEndDay(endDay);
                }   
            }                                                                               
            input.nextLine();
            System.out.println("Please type the place which the tournament will be held:");
            String place = input.nextLine();
            myTournament.setPlace(place);
        
            System.out.println("The tournament "+myTournament.getName()+" has started!\n" );
           
            startYear = myTournament.getTournamentStartYear();          //Required for checks below.
            startMonth = myTournament.getTournamentStartMonth();        //Required for checks below.
            startDay = myTournament.getTournamentStartDay();            //Required for checks below.
            endYear = myTournament.getTournamentEndYear();              //Required for checks below.
            endMonth = myTournament.getTournamentEndMonth();            //Required for checks below.
            endDay = myTournament.getTournamentEndDay();                //Required for checks below.
    }
  
    public void CreateGame(ArrayList<Game> Games, ArrayList<Participant> Participants){  //Creates a game and puts it in the arraylist.After that it creates the participants of the game.
        
        
            System.out.println("**Game Creation**");
           
            System.out.println("Please type the name for the Game:");
            Scanner input = new Scanner(System.in);
            String name = input.nextLine();
            myGame.setName(name);
            
            myGame.setGameStatus(0);
            
            while ( (myGame.getGameStatus() != 1) && (myGame.getGameStatus() != 2) ){
                System.out.println("Give the type for "+myGame.getName()+" game.Type 1 for TimeGame or Type 2 for RoundGame:"); 
                int type = input.nextInt();
                myGame.setGameStatus(type);   
             }
            
            if (myGame.getGameStatus()==1){     
                myGame.setType("Time Game");  
            }else{
                myGame.setType("Round Game");  
            }            
             
            int z=0;            
            do{
                System.out.println("Please type the number of players(without the administrator) for "+myGame.getName()+" game (Power of 2):");
                int players = input.nextInt(); 
                myGame.setMaxParticipants(players);
                int y= myGame.getMaxParticipants();
                z=0;
                for (int i=0; i<myGame.getMaxParticipants()/2; i++){        //Checks if the maximum number of participants is power of 2.
                    if (y==1){
                        i= myGame.getMaxParticipants()/2 -1;
                    }else{
                        y/=2;                 
                        if(y!=1){
                            if(y%2==1){
                                z=1;
                            }
                        }
                    }
                }
                if(z==0){
                    participants = myGame.getMaxParticipants();
                } 
            }while(z==1);
            participants++;
                
            System.out.println("Please type the date which the competition will start:");       
            
            System.out.println("Year ("+startYear+"-"+endYear+") :");
            int StartYear = input.nextInt();
            myGame.setGameStartYear(StartYear);
            while(myGame.getGameStartYear()<startYear || myGame.getGameStartYear()>endYear) {
                System.out.println("Wrong input,please try again!\n "+startYear+"-"+endYear);
                StartYear = input.nextInt();
                myGame.setGameStartYear(StartYear);
            }
            
            System.out.println("Month:");
            int StartMonth = input.nextInt();
            myGame.setGameStartMonth(StartMonth);
            if(myGame.getGameStartYear()==startYear || myGame.getGameStartYear()==endYear){
                while(myGame.getGameStartMonth()<startMonth && myGame.getGameStartMonth()>endMonth || myGame.getGameStartMonth()>12) {
                    System.out.println("Wrong input,please try again!\n"+startMonth+"-"+endMonth);
                    StartMonth = input.nextInt();
                    myGame.setGameStartMonth(StartMonth);
                }
            }else{
                while(myGame.getGameStartMonth()<=0 || myGame.getGameStartMonth()>12) {
                    System.out.println("Wrong input,please try again!\n (between 1-12)");
                    StartMonth = input.nextInt();
                    myGame.setGameStartMonth(StartMonth);
                }
            }
                        
            System.out.println("Day:");
            int StartDay = input.nextInt();
            myGame.setGameStartDay(StartDay);
            if(myGame.getGameStartYear()==startYear || myGame.getGameStartYear()==endYear){
                if(myGame.getGameStartMonth()==startMonth || myGame.getGameStartMonth()==endMonth){
                    while(myGame.getGameStartDay()<startDay && myGame.getGameStartDay()>endDay) {
                        System.out.println("Wrong input,please try again!\n "+startDay+"-"+endDay);
                        StartDay = input.nextInt();
                        myGame.setGameStartDay(StartDay);
                    }   
                }
            }else{
                while(myGame.getGameStartDay()<=0 || myGame.getGameStartDay()>31) {
                    System.out.println("Wrong input,please try again!\n(between 0-31)");
                    StartDay = input.nextInt();
                    myGame.setGameStartDay(StartDay);
                }
            }
         
            System.out.println("Please type the time which the competition will start:");
            System.out.println("Hour (00-23):");
            int startHour = input.nextInt();
            myGame.setGameStartHour(startHour);
            while(myGame.getGameStartHour()<0 || myGame.getGameStartHour()>23){
                System.out.println("Wrong input,please try again!\n (00-23)");
                startHour = input.nextInt();
                myGame.setGameStartHour(startHour);
            }
            
            System.out.println("Minutes (00-59) :");
            int startMinutes = input.nextInt();
            myGame.setGameStartMinutes(startMinutes);
            while(myGame.getGameStartMinutes()<0 || myGame.getGameStartMinutes()>59){
                System.out.println("Wrong input,please try again!\n (00-59) ");
                startMinutes = input.nextInt();
                myGame.setGameStartMinutes(startMinutes);
            }
            
            if(myGame.getType().equals("Time Game")){       //If the game is timegame,it copies the data and adds it to the arraylist.
                input.nextLine();
                System.out.println("Please provide the time that the game will last in minutes:" ); 
                int Minutes = input.nextInt();
                myTimeGame.setMinutes(Minutes);
                while(myTimeGame.getMinutes()<0 || myTimeGame.getMinutes() > 240 ){
                    System.out.println("Wrong input,please try again!\n (0-240)");      //The time that the game will last.
                    Minutes = input.nextInt();
                    myTimeGame.setMinutes(Minutes);
                }
                myTimeGame.setName(myGame.getName());
                myTimeGame.setType(myGame.getType());
                myTimeGame.setGameStatus(myGame.getGameStatus());
                myTimeGame.setMaxParticipants(myGame.getMaxParticipants());
                myTimeGame.setGameStartDay(myGame.getGameStartDay());
                myTimeGame.setGameStartMonth(myGame.getGameStartMonth());
                myTimeGame.setGameStartYear(myGame.getGameStartYear());
                myTimeGame.setGameStartHour(myGame.getGameStartHour());
                myTimeGame.setGameStartMinutes(myGame.getGameStartMinutes());
            
            System.out.println("The game "+myTimeGame.getName()+ " has just been created!" ); 
            
            Games.add(new TimeGame(myTimeGame.getMinutes(),myTimeGame.getName(),myTimeGame.getType(),myTimeGame.getMaxParticipants(),myTimeGame.getGameStartDay(),myTimeGame.getGameStartMonth(),myTimeGame.getGameStartYear(),myTimeGame.getGameStartHour(),myTimeGame.getGameStartMinutes(),myTimeGame.getGameStatus()));
            }
            if(myGame.getType().equals("Round Game")){      //If the game is roundgame,it copies the data and adds it to the arraylist.
                input.nextLine();
                System.out.println("The winner is decided after 3 wins");
                myRoundGame.setName(myGame.getName());
                myRoundGame.setType(myGame.getType());
                myRoundGame.setGameStatus(myGame.getGameStatus());
                myRoundGame.setMaxParticipants(myGame.getMaxParticipants());
                myRoundGame.setGameStartDay(myGame.getGameStartDay());
                myRoundGame.setGameStartMonth(myGame.getGameStartMonth());
                myRoundGame.setGameStartYear(myGame.getGameStartYear());
                myRoundGame.setGameStartHour(myGame.getGameStartHour());
                myRoundGame.setGameStartMinutes(myGame.getGameStartMinutes());
            
            System.out.println("The game "+myRoundGame.getName()+ " has just been created!" ); 
            
            Games.add(new RoundGame(myRoundGame.getName(),myRoundGame.getType(),myRoundGame.getMaxParticipants(),myRoundGame.getGameStartDay(),myRoundGame.getGameStartMonth(),myRoundGame.getGameStartYear(),myRoundGame.getGameStartHour(),myRoundGame.getGameStartMinutes(),myRoundGame.getGameStatus()));
                
                
                
                
            }
            
            System.out.println("\n");
            System.out.println("   (Name)\t\t(Type)\t(MaxParticipants)\tStarts at:");
            for(int i=0; i<Games.size(); i++){                            
                            System.out.printf("%s %20s %40d\t%02d:%02d \0 %d/%d/%d \n\n",Games.get(i).getName(),Games.get(i).getType(),Games.get(i).getMaxParticipants(),+Games.get(i).getGameStartHour(),Games.get(i).getGameStartMinutes(),Games.get(i).getGameStartDay(),Games.get(i).getGameStartMonth(),Games.get(i).getGameStartYear());
            }    
           checkAdministrator = 0;
           howManyPlayers = 0;
           GameName=myGame.getName();                   //Creates the participants for the new game.
           for(int i=0; i<participants; i++){
                CreateParticipant(Participants,0,GameName);
            }
    }   
    
    void EditGame(ArrayList<Game> Games){       //Edits a game that is already in the arraylist.
         Scanner input = new Scanner(System.in);
         int size = Games.size() - 1;
         int choice;
         do{ System.out.println("Which game you would like to edit?");      //The user chooses a game.
                        for(int i=0; i<Games.size(); i++){                            
                           System.out.printf("\nGame %d %s\t%s\t%d\t\t\t%02d:%02d\0%d/%d/%d \n\n",i,Games.get(i).getName(),Games.get(i).getType(),Games.get(i).getMaxParticipants(),+Games.get(i).getGameStartHour(),Games.get(i).getGameStartMinutes(),Games.get(i).getGameStartDay(),Games.get(i).getGameStartMonth(),Games.get(i).getGameStartYear());
                        }
                            choice = input.nextInt();
                            if(choice >= 0 && choice <=size){
                                System.out.printf("\nGame %d %s\t%s\t%d\t\t\t%02d:%02d\0%d/%d/%d \n\n",choice,Games.get(choice).getName(),Games.get(choice).getType(),Games.get(choice).getMaxParticipants(),+Games.get(choice).getGameStartHour(),Games.get(choice).getGameStartMinutes(),Games.get(choice).getGameStartDay(),Games.get(choice).getGameStartMonth(),Games.get(choice).getGameStartYear());
                                input.nextLine(); 
                                System.out.println("Please type the name for the Game:");
                                String name = input.nextLine();
                                  Games.get(choice).setName(name);
            
                                  Games.get(choice).getGameStatus();
            
                                 while ( (Games.get(choice).getGameStatus() != 1) && (Games.get(choice).getGameStatus() != 2) ){
                                        System.out.println("Give the type for "+Games.get(choice).getName()+" game.Type 1 for TimeGame or Type 2 for RoundGame:"); 
                                        int type = input.nextInt();
                                         Games.get(choice).setGameStatus(type);   
                                 }
            
                                 if (Games.get(choice).getGameStatus()==1){
                                        Games.get(choice).setType("Time Game");  
                                }else{
                                        Games.get(choice).setType("Round Game");  
                                }            
             
                                int z=0;            
                                do{
                                     System.out.println("Please type the number of players(without the administrator) for "+Games.get(choice).getName()+" game (Power of 2):");
                                     int players = input.nextInt(); 
                                    Games.get(choice).setMaxParticipants(players);
                                    int y= Games.get(choice).getMaxParticipants();
                                    z=0;
                                    for (int i=0; i<Games.get(choice).getMaxParticipants()/2; i++){
                                         if (y==1){
                                                i= Games.get(choice).getMaxParticipants()/2 -1;
                                         }else{
                                            y/=2;                 
                                            if(y!=1){
                                                if(y%2==1){
                                                     z=1;
                                                }
                                             }
                                        }
                                    }
                                     if(z==0){
                                         participants = Games.get(choice).getMaxParticipants();
                                    } 
                                }while(z==1);
                                     participants++;
                
            System.out.println("Please type the date which the competition will start:");
            
            System.out.println("Year ("+startYear+"-"+endYear+") :");
            int StartYear = input.nextInt();
            Games.get(choice).setGameStartYear(StartYear);
            while(Games.get(choice).getGameStartYear()<startYear || Games.get(choice).getGameStartYear()>endYear) {
                System.out.println("Wrong input,please try again!\n "+startYear+"-"+endYear);
                StartYear = input.nextInt();
                Games.get(choice).setGameStartYear(StartYear);
            }
            
            System.out.println("Month:");
            int StartMonth = input.nextInt();
            Games.get(choice).setGameStartMonth(StartMonth);
            if(Games.get(choice).getGameStartYear()==startYear || Games.get(choice).getGameStartYear()==endYear){
                while(Games.get(choice).getGameStartMonth()<startMonth && Games.get(choice).getGameStartMonth()>endMonth || Games.get(choice).getGameStartMonth()>12) {
                    System.out.println("Wrong input,please try again!\n"+startMonth+"-"+endMonth);
                    StartMonth = input.nextInt();
                    Games.get(choice).setGameStartMonth(StartMonth);
                }
            }else{
                while(Games.get(choice).getGameStartMonth()<=0 || Games.get(choice).getGameStartMonth()>12) {
                    System.out.println("Wrong input,please try again!\n(between 1-12)");
                    StartMonth = input.nextInt();
                    Games.get(choice).setGameStartMonth(StartMonth);
                }
            }
                        
            System.out.println("Day:");
            int StartDay = input.nextInt();
            Games.get(choice).setGameStartDay(StartDay);
            if(Games.get(choice).getGameStartYear()==startYear || Games.get(choice).getGameStartYear()==endYear){
                if(Games.get(choice).getGameStartMonth()==startMonth || Games.get(choice).getGameStartMonth()==endMonth){
                    while(Games.get(choice).getGameStartDay()<startDay || Games.get(choice).getGameStartDay()>endDay) {
                        System.out.println("Wrong input,please try again!\n"+startDay+"-"+endDay);
                        StartDay = input.nextInt();
                        Games.get(choice).setGameStartDay(StartDay);
                    }   
                }
            }else{
                while(Games.get(choice).getGameStartDay()<=0 || Games.get(choice).getGameStartDay()>31) {
                    System.out.println("Wrong input,please try again!\n(between 0-31)");
                    StartDay = input.nextInt();
                    Games.get(choice).setGameStartDay(StartDay);
                }
            }
         
            System.out.println("Please type the time which the competition will start:");
            System.out.println("Hour (00-23):");
            int startHour = input.nextInt();
            Games.get(choice).setGameStartHour(startHour);
            while(Games.get(choice).getGameStartHour()<0 || Games.get(choice).getGameStartHour()>23){
                System.out.println("Wrong input,please try again!\n(00-23)");
                startHour = input.nextInt();
                Games.get(choice).setGameStartHour(startHour);
            }
            
            System.out.println("Minutes (00-59) :");
            int startMinutes = input.nextInt();
            Games.get(choice).setGameStartMinutes(startMinutes);
            while(Games.get(choice).getGameStartMinutes()<0 || Games.get(choice).getGameStartMinutes()>59){
                System.out.println("Wrong input,please try again!\n (00-59)");
                startMinutes = input.nextInt();
                Games.get(choice).setGameStartMinutes(startMinutes);
            }
            
            
            System.out.println("The game "+Games.get(choice).getName()+ " has just been edited!" ); 
            
            
            
            System.out.println("\n");
            System.out.println("(Name)\t\t(Type)\t(MaxParticipants)\tStarts at:");
            for(int i=0; i<Games.size(); i++){                            
                            System.out.printf("\nGame %d %s\t%s\t%d\t\t\t%02d:%02d\0%d/%d/%d \n\n",i,Games.get(i).getName(),Games.get(i).getType(),Games.get(i).getMaxParticipants(),+Games.get(i).getGameStartHour(),Games.get(i).getGameStartMinutes(),Games.get(i).getGameStartDay(),Games.get(i).getGameStartMonth(),Games.get(i).getGameStartYear());
                            }
            }else{
              System.out.println("Wrong input\n");
             }
        }while(choice < 0 || choice > size);
    }
    
    void CreateParticipant(ArrayList<Participant> Participants,int counter,String GameName) {   //Creates a participant.
        if(counter==1){     //Sets these values,if the game has just been created.
            checkAdministrator = 0;
            howManyPlayers = 0;
        }
       
       System.out.println("**Player/Administrator Creation**");
       
       int choice=0;
               
       Scanner input = new Scanner(System.in);
       if (checkAdministrator==0){      //If the game doesn't have an administrator.
           if(howManyPlayers==participants-1){  //If all the players are added,now add an administrator.
           do{        
                System.out.println("Type 2 for administrator");
                choice = input.nextInt();
                myAdmin.setPType(choice);       
            }while(choice!=2);  
            }else{   
            do{        
                System.out.println("Would you like to create a player or an administrator?");
                System.out.println("Type 1 for player,2 for administrator:");              
                choice = input.nextInt();                   
            }while(choice==1 && choice==2);
            howManyPlayers++;
            }
       }else{
            do{        
                System.out.println("Type 1 for player");
                choice = input.nextInt();
                myPlayer.setPType(choice);       
            }while(choice!=1);  
        }        
        if (choice==2){     //If the game is already in the list,then it already has an administrator.
            checkAdministrator=1;
        }
        
        input.nextLine(); 
        System.out.println("Please type a name:");
        String name = input.nextLine();
        myParticipant.setPName(name);
        
        System.out.println("Please type a nickname for "+name+" :");       
        String nickname = input.nextLine();
        myParticipant.setNickname(nickname);
        
        System.out.println("");
       
        if(choice == 1){        //If the participant is a player.
        myPlayer.setPType(1);
        myPlayer.setPName(myParticipant.getPName());            //Copy the previously inserted data.
        myPlayer.setNickname(myParticipant.getNickname());
       
            boolean correctMail1;
            boolean correctMail2;
            do{
                System.out.println("Please type the e-mail for the Player "+name+" (mail must contain '@' and '.com'):");       
                String mail = input.nextLine();
                myPlayer.setCommunication(mail);        //Check if the email has @ and .com
                myPlayer.setEmail(mail);
                
                CharSequence at = "@";
                CharSequence dotCom = ".com";            //Check if the email has @ and .com
                correctMail1= mail.contains(at);
                correctMail2= mail.contains(dotCom);
            }while(correctMail1!=true || correctMail2!=true);
            myPlayer.setGame(GameName);
            Participants.add(new Player(myPlayer.getEmail(),myPlayer.getPName(),myPlayer.getNickname(),myPlayer.getPType(),myPlayer.getCommunication(),myPlayer.getGame()));
        }else{          //If the participant is an administrator.
        myAdmin.setPType(choice);
        myAdmin.setPName(myParticipant.getPName());
        myAdmin.setNickname(myParticipant.getNickname());
        
            int countPhonenumber=0;
            myAdmin.setPType(2);
            do{
                System.out.println("Please type a phone number for the Administrator "+name+" (phone number must contain ten numbers):");       
                String phonenumber = input.nextLine();
                String mail ="null";                    //Check if the phonenumber has 10 digits.
                myAdmin.setCommunication(phonenumber);
                myAdmin.setPhoneNumber(phonenumber);
   
                for(int i=0; i<phonenumber.length(); i++){
                    countPhonenumber++;
                }
            }while(countPhonenumber!=10);
            myAdmin.setGame(GameName);
            Participants.add(new Administrator(myAdmin.getPhoneNumber(),myAdmin.getPName(),myAdmin.getNickname(),myAdmin.getPType(),myAdmin.getCommunication(),myAdmin.getGame()));
        }       
        
       
        
        System.out.println("\n");
        System.out.println("(Game)\t(Name)\t(Nickname)\t(Type)\t(Communication)");
        for (int i=0; i<Participants.size(); i++){
            System.out.println(Participants.get(i).getGame()+"\t"+Participants.get(i).getPName()+"\t"+Participants.get(i).getNickname() +"\t"+Participants.get(i).getPType()+"\t"+Participants.get(i).getCommunication());               
        }
        System.out.println("\n");
        
        ParticipantCount++; //Increase by one every time a participant is added to the arraylist.
            
    }  
    
    void EditParticipant(ArrayList<Participant> Participants){      //Edits a participant that has already been added to the arraylist.
        Scanner input = new Scanner(System.in);
        int size = Participants.size() - 1;
        int choice;
        int selection;
        System.out.println("Which participant would you like to edit?");            //The user chooses a participant.
        System.out.println("\n");
        do{System.out.println("Number\t(Name)\t(Nickname)\t(Type)\t(Communication)");
            for (int i=0; i<Participants.size(); i++){
            System.out.println("\0"+ Participants.get(i).getPName()+"\t"+Participants.get(i).getNickname() +"\t"+Participants.get(i).getPType()+"\t"+Participants.get(i).getCommunication());               
            }
            choice = input.nextInt();
            if(choice >= 0 && choice <=size){
                System.out.println(Participants.get(choice)+"\0"+ Participants.get(choice).getPName()+"\t"+Participants.get(choice).getNickname() +"\t"+Participants.get(choice).getPType()+"\t"+Participants.get(choice).getCommunication());               
            }   
        input.nextLine();    
        System.out.println("Please type a name:");       
        String name = input.nextLine();
        Participants.get(choice).setPName(name);
       
       
        System.out.println("Please type a nickname for "+name+" :");       
        String nickname = input.nextLine();
        Participants.get(choice).setNickname(nickname);
       
        if(Participants.get(choice).getPType() == 1){   //If the participant is a player...
            boolean correctMail1;
            boolean correctMail2;
            do{
                System.out.println("Please type the e-mail for the Player "+name+" (mail must contain '@' and '.com'):");       
                String mail = input.nextLine();
                Participants.get(choice).setCommunication(mail);
                
                CharSequence at = "@";
                CharSequence dotCom = ".com";
                correctMail1= mail.contains(at);
                correctMail2= mail.contains(dotCom);
            }while(correctMail1!=true || correctMail2!=true);
        }else{              //If the participant is an administrator.
            int countPhonenumber=0;
            Participants.get(choice).setPType(2);
            do{
                System.out.println("Please type a phone number for the Administrator "+name+" (phone number must contain ten numbers):");       
                String phonenumber = input.nextLine();
                String mail ="null";
                Participants.get(choice).setCommunication(phonenumber);                
   
                for(int i=0; i<phonenumber.length(); i++){
                    countPhonenumber++;
                }
            }while(countPhonenumber!=10);
        }       
        
        System.out.println("\n");
        System.out.println("(Name)\t(Nickname)\t(Type)\t(Communication)");
        for (int i=0; i<Participants.size(); i++){
            System.out.println( Participants.get(i).getPName()+"\t"+Participants.get(i).getNickname() +"\t"+Participants.get(i).getPType()+"\t"+Participants.get(i).getCommunication());               
        }
        }while(choice < 0 || choice > size);
        System.out.println("\n");
    }

}   

