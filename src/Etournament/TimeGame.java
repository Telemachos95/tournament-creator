
package Etournament;
/**
 *
 * @authors it21338,it21374
 */
public class TimeGame extends Game {         //The TimeGame class inherits all the fields and methods of class Game.
    private int Minutes;        //The time the each game will last in minutes.

    public TimeGame(int Minutes, String Name, String Type, int MaxParticipants, int GameStartDay, int GameStartMonth, int GameStartYear, int GameStartHour, int GameStartMinutes, int GameStatus) {     //Constructor for TimeGame class.
        super(Name, Type, MaxParticipants, GameStartDay, GameStartMonth, GameStartYear, GameStartHour, GameStartMinutes, GameStatus);
        this.Minutes = Minutes;
    }

    public int getMinutes() {       //Returns the minutes.
        return Minutes;
    }

    public void setMinutes(int Minutes) {       //Sets the minutes.
        this.Minutes = Minutes;
    }
    
    
    
}
