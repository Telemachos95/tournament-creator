
package Etournament;
/**
 *
 * @authors it21338,it21374
 */
public class Tournament {       
   private String Name;     //Tournament's name.
   private int TournamentStartDay;      //The day the tournament will start.
   private int TournamentStartMonth;    //The month the tournament will start.
   private int TournamentStartYear;     //The year the tournament will start.
   private int TournamentEndDay;        //The day the tournament will end.
   private int TournamentEndMonth;      //The month the tournament will end.
   private int TournamentEndYear;       //The year the tournament will end.
   private String Place;                //The place the tournament will take place.

    public Tournament() {       //Constructor for Tournament class.
        this.Name = "";
        this.TournamentStartDay = 0;
        this.TournamentStartMonth = 0;
        this.TournamentStartYear = 0;
        this.TournamentEndDay = 0;
        this.TournamentEndMonth = 0;
        this.TournamentEndYear = 0;
        this.Place = "";
    }

    public String getName() {       //Returns the name.
        return Name;
    }

    public void setName(String Name) {  //Sets the name.
        this.Name = Name;
    }

    public int getTournamentStartDay() {    //...
        return TournamentStartDay;
    }

    public void setTournamentStartDay(int TournamentStartDay) { //...
        this.TournamentStartDay = TournamentStartDay;
    }

    public int getTournamentStartMonth() {      //...
        return TournamentStartMonth;
    }

    public void setTournamentStartMonth(int TournamentStartMonth) {     //...
        this.TournamentStartMonth = TournamentStartMonth;
    }

    public int getTournamentStartYear() {       //...
        return TournamentStartYear;
    }

    public void setTournamentStartYear(int TournamentStartYear) {       //...
        this.TournamentStartYear = TournamentStartYear;
    }

    public int getTournamentEndDay() {      //...
        return TournamentEndDay;
    }

    public void setTournamentEndDay(int TournamentEndDay) {     //...
        this.TournamentEndDay = TournamentEndDay;
    }

    public int getTournamentEndMonth() {        //...
        return TournamentEndMonth;
    }

    public void setTournamentEndMonth(int TournamentEndMonth) {     //...
        this.TournamentEndMonth = TournamentEndMonth;
    }

    public int getTournamentEndYear() {     //...
        return TournamentEndYear;
    }

    public void setTournamentEndYear(int TournamentEndYear) {       //...
        this.TournamentEndYear = TournamentEndYear;
    }

    public String getPlace() {      //...
        return Place;
    }

    public void setPlace(String Place) {        //...
        this.Place = Place;
    }
    
    
    
}


