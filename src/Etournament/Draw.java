package Etournament;
/**
 *
 * @authors it21338,it21374
 */
import java.util.Scanner;   //Required in order to take input from the keyboard.
import java.util.ArrayList; //Required in order to create an Arraylist.
import java.util.Random;    //Required in order to use the Random Class.

public class Draw {       
    
    public int adminFinder;
    public int counter ;
    
    public void draw(int choice, ArrayList<Game> Games, ArrayList<Participant> Participants, int number[][] ){
        Random randomDraw = new Random();
        
        int element, winner = 0, k = 0, player1=0, player2=0, exist=0; 
        
        int sum=0;                                                                           //sum= o 1os paixtis/admin sti lista tou game
        
        Scanner input = new Scanner(System.in);
        for(int i=0; i<Games.size(); i++){
                System.out.printf("\nGame %d %s\t%s\t%d\t\t\t%02d:%02d\0%d/%d/%d \n\n",i,Games.get(i).getName(),Games.get(i).getType(),Games.get(i).getMaxParticipants(),+Games.get(i).getGameStartHour(),Games.get(i).getGameStartMinutes(),Games.get(i).getGameStartDay(),Games.get(i).getGameStartMonth(),Games.get(i).getGameStartYear());
        }
        do{
            System.out.println("Please provide a game:");            
            element = input.nextInt();
            for(int i=0; i<Games.size(); i++){
                System.out.println(Games.get(i).getName()+"\t"+Games.get(i).getType()+"\t"+Games.get(i).getMaxParticipants()+"\t\t\t"+Games.get(i).getGameStartHour()+":"+Games.get(i).getGameStartMinutes()+" \0 "+Games.get(i).getGameStartDay()+"/"+Games.get(i).getGameStartMonth()+"/"+Games.get(i).getGameStartYear()+"\n");
                if(element==i){
                    k=1;
                }
            }
        }while(k!=1);                                                          
                   
        if (choice==1){
            for(int i=0; i<Games.get(element).getMaxParticipants(); i++){              
                exist=0;
                
                if(number[element][i]==0){
                    number[element][i] = randomDraw.nextInt(Games.get(element).getMaxParticipants()) +1;    
                }else{
                    for(int j=0; i<Games.get(element).getMaxParticipants(); j++){
                        if(i!=j){
                            if(number[element][i]==j+1){
                                exist=1;
                            }
                        }
                    }
                
                    if(exist==1){
                        number[element][i] = randomDraw.nextInt(Games.get(element).getMaxParticipants()) +1; 
                    }else{
                        number[element][i] = 1+ randomDraw.nextInt(Games.get(element).getMaxParticipants()) +Games.get(element).getMaxParticipants(); 
                    }
                }
            }
        
        
            for(int i=0; i<Games.get(element).getMaxParticipants(); i++){
                for(int j=0; j<Games.get(element).getMaxParticipants(); j++){
                    if(i!=j){
                        if(number[element][i] == number[element][j]){
                            number[element][j] = randomDraw.nextInt(Games.get(element).getMaxParticipants()) +1;
                            j= -1;
                            i= 0;
                        }
                    }
                }
            }
            
            for(int j=0; j<element; j++){
                    if(element==0){
                        sum=0;
                    }else{
                        sum+= Games.get(j).getMaxParticipants();
                        
                    }
                }
                sum+=element;                                                                             //pros8etw kai tous admins                                                                 
                
            
                boolean mail;
                CharSequence at = "@";
            for(int i=0; i<Games.get(element).getMaxParticipants(); i++){                           //Vlepw pou vrisketai o admin                                              
                    mail= Participants.get(sum+i).getCommunication().contains(at);
                    if(mail==false){    
                        adminFinder=i;
                        
                    }
                     
                }
            
        }else if(choice==2){
            for(int i=0; i<Games.get(element).getMaxParticipants(); i++){                
                if(i%2==1){
                    player1=0;  player2=0; 
                    if(Games.get(element).getGameStatus()== 1){
                    do{
                        System.out.println("Please provide the winners for each game:");
                        System.out.println(number[element][i]+" vs "+number[element][i-1]);
                        winner = input.nextInt();
                    }while(winner!=number[element][i] && winner!=number[element][i-1]);
                        System.out.println("And the winner is... Player"+winner);
                        number[element][i/2]= winner;
                                 
                    }else{
                        do{
                            System.out.println("Please provide the winners for each game:");
                            System.out.println(number[element][i]+" vs "+number[element][i-1]);
                            winner = input.nextInt();
                            if(winner==number[element][i]){
                                player1++;
                            }else if(winner==number[element][i-1]){
                                player2++;
                            }else{
                                System.out.println("Type: "+number[element][i]+" or "+number[element][i-1]);
                            }    
                            
                            System.out.println("WINS: "+"Player"+number[element][i]+": "+player1 +"  Player"+number[element][i-1]+": "+ player2);
                            
                            if(player1==3){
                                System.out.println();
                                System.out.println("The Winner is... Player"+number[element][i]);
                            }else if(player2==3){
                                System.out.println();
                                System.out.println("The Winner is... Player"+number[element][i-1]);
                            }else{
                                
                            }
                        }while(player1!=3 && player2!=3);

                        if(player1==3){
                            number[element][i/2] =number[element][i];
                        }else{
                            number[element][i/2] =number[element][i-1];
                        }
                    }
                    
                }                
            }
            Games.get(element).setMaxParticipants(Games.get(element).getMaxParticipants()/2);
        }else{                                    
            if(choice==3){
                for(int i=0; i<Games.get(element).getMaxParticipants(); i++){
                    if(i%2==1){
                        System.out.println(number[element][i] + " " + Participants.get(counter).getPName()+"   plays with   "+number[element][i-1] + " " + Participants.get(counter).getGame());
                        System.out.println(); 
                    }
                }
            }else{                    
                for(int i=0; i<Games.get(element).getMaxParticipants(); i++){                           //Mexri ekei pou vriskete o admin vazw sta stoixeia -1, dil to 1 vriskete sti 8esi 0, to 2 stin 1 kok
                    counter= sum+number[element][i]-1;
                                        
                    if(number[element][i] > adminFinder){                                                                //An den einai o admin
                        ++counter;                        
                    }
                                
                    System.out.println(number[element][i] + " " + Participants.get(counter).getPName() + " " + Participants.get(counter).getNickname() + " " + Participants.get(counter).getCommunication());
                    System.out.println();
                }                            
            }         
        }        
    }           
}

