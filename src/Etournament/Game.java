
package Etournament;
/**
 *
 * @authors it21338,it21374
 */
public class Game {
        private String Name;    //The name of the game.
        private String Type;    //The type of the game is either "Time Game" or "Round Game".
        private int MaxParticipants;    //Displays the maximum number of players for a particular game(excluding the administrator).
        private int GameStartDay;       //The day the game will start.
        private int GameStartMonth;     //The month the game will start.
        private int GameStartYear;      //The year the game will start.
        private int GameStartHour;      //The hour the game will start.
        private int GameStartMinutes;   //The minutes the game will start.
        private int GameStatus;         //The GameStatus is either 1 for a Timegame,or 2 for a Roundgame.

    public Game() {                     //Primary Constructor for the Game class which has its variables initialised.It is used mainly to create a game object.
        this.Name = "";
        this.Type = "";
        this.MaxParticipants = 0;
        this.GameStartDay = 0;
        this.GameStartMonth = 0;
        this.GameStartYear = 0;
        this.GameStartHour = 0;
        this.GameStartMinutes = 0;
        this.GameStatus = 0;
    }

    public Game(String Name, String Type, int MaxParticipants, int GameStartDay, int GameStartMonth, int GameStartYear, int GameStartHour, int GameStartMinutes, int GameStatus) { //Secondary Constructor.
        this.Name = Name;
        this.Type = Type;
        this.MaxParticipants = MaxParticipants;
        this.GameStartDay = GameStartDay;
        this.GameStartMonth = GameStartMonth;
        this.GameStartYear = GameStartYear;
        this.GameStartHour = GameStartHour;
        this.GameStartMinutes = GameStartMinutes;
        this.GameStatus = GameStatus;
    }

    public Game(String Name) {  //Third Constructor
        this.Name = Name;
    }
    
    
    
    

    public String getName() {       //Returns the name.
        return Name;
    }

    public void setName(String Name) {  //Sets the name.
        this.Name = Name;
    }

    public String getType() {       //Returns the type.
        return Type;
    }

    public void setType(String Type) {  //Sets the type.
        this.Type = Type;
    }

    public int getMaxParticipants() {   //...
        return MaxParticipants;
    }

    public void setMaxParticipants(int MaxParticipants) {   //...
        this.MaxParticipants = MaxParticipants;
    }

    public int getGameStartDay() {      //...
        return GameStartDay;
    }

    public void setGameStartDay(int GameStartDay) {     //...
        this.GameStartDay = GameStartDay;
    }

    public int getGameStartMonth() {                //...
        return GameStartMonth;
    }

    public void setGameStartMonth(int GameStartMonth) {     //...
        this.GameStartMonth = GameStartMonth;
    }

    public int getGameStartYear() {                 //...
        return GameStartYear;
    }

    public void setGameStartYear(int GameStartYear) {       //...
        this.GameStartYear = GameStartYear;
    }

    public int getGameStartHour() {         //...
        return GameStartHour;
    }

    public void setGameStartHour(int GameStartHour) {       //...
        this.GameStartHour = GameStartHour;
    }

    public int getGameStartMinutes() {          //...
        return GameStartMinutes;
    }

    public void setGameStartMinutes(int GameStartMinutes) {     //...
        this.GameStartMinutes = GameStartMinutes;
    }
    
     public int getGameStatus() {               //...
            return GameStatus;
        }

        public void setGameStatus(int GameStatus) {         //...
            this.GameStatus = GameStatus;
        }
      
      
}
   
    
    

    
  
        

