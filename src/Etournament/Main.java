package Etournament;
/**
 *
 * @authors it21338,it21374
 */
import java.util.Scanner;   //Required in order to take input from the keyboard.
import java.util.ArrayList; //Required in order to create an Arraylist.
public class Main {
    

    public static void main(String[] args) {
      Options NewTournament = new Options();        //Creates an Options object.
      ArrayList<Game> Games = new ArrayList<Game>();    //Creates an Arraylist of type Game.
      ArrayList<Participant> Participants = new ArrayList<Participant>();   //Creates an Arraylist of type Participant.
      NewTournament.StartTournament();      //The Options object calls the StartTournament method.
      
       System.out.println("You need to assign at least one game for this tournament:");
       
       NewTournament.CreateGame(Games,Participants);    //The Options object calls the CreateGame method.

       Draw draw = new Draw();                  //Creates a Draw object.
       int number[][] =new int[5][64];          //Creates a two dimensional array which includes the games,as well as the participants.It is clear that the games are limited to 5 and the participants to 64.
       
       int choice;      //Used to take the user's choice.
       do{
            System.out.println("1. Add/Edit/Delete a Game:");
            System.out.println("2. Add/Edit/Delete a Participant:");        //The available options of the program.
            System.out.println("3. Draw/Add Results:");
            System.out.println("4. Print the Draw/Results:");
            System.out.println("5. Exit\n");
       
            Scanner input = new Scanner(System.in);
            choice = input.nextInt();
            
            if(choice==1){
                do{
                    System.out.println("1. Add a Game:");
                    System.out.println("2. Edit a Game:");
                    System.out.println("3. Delete a Game:");
                    System.out.println("4. Return\n");
                    choice = input.nextInt();
            
                    if(choice ==1){
                        NewTournament.CreateGame(Games,Participants);   //Call the method that creates a game and puts it in the arraylist.
                    }else if(choice==2){
                        NewTournament.EditGame(Games);      //Edits a game that is already in the arraylist.
                    }else if(choice==3){
                         int size = Games.size() - 1;        
                        if(size == -1){         //Checks if the games arraylist is empty.If so,it then creates a new game.
                                System.out.println("It seems like the Games list is empty.You need to assign at least one game for this tournament:");
                                NewTournament.CreateGame(Games,Participants);
                            }
                          do{  
                            for(int i=0; i<Games.size(); i++){                            
                            System.out.printf("\nGame %d %s\t%s\t%d\t\t\t%02d:%02d\0%d/%d/%d \n\n",i,Games.get(i).getName(),Games.get(i).getType(),Games.get(i).getMaxParticipants(),+Games.get(i).getGameStartHour(),Games.get(i).getGameStartMinutes(),Games.get(i).getGameStartDay(),Games.get(i).getGameStartMonth(),Games.get(i).getGameStartYear());
                            }
                            System.out.println("Please type the game you wish to remove(Type between Game 0-"+size+":");
                            choice = input.nextInt();
                            if(choice >= 0 && choice <=size){
                                System.out.println(Games.get(choice).getName()+" has been deleted!\n");
                                Games.remove(choice);       //After printing the games that are currently in the arraylist,it then removes the game that the user chose.
                            }
                          }while(choice < 0 || choice > size);
                            
                    }else if(choice==4){
                
                    }else{
                        System.out.println("Wrong input,please try again!\n");
                    }                                
                }while(choice!=4);
                
            }else if(choice==2){
                do{
                    System.out.println("1. Add a Participant");
                    System.out.println("2. Edit a Participant");
                    System.out.println("3. Delete a Participant");
                    System.out.println("4. Return\n");    
                    choice = input.nextInt();
            
                    if(choice ==1){
                        int size = Games.size() - 1;
                        String GameName;
                        System.out.println("In which game would you like to add this player?");
                         do{  
                            for(int i=0; i<Games.size(); i++){                            
                            System.out.printf("\nGame %d %s\t%s\t%d\t\t\t%02d:%02d\0%d/%d/%d \n\n",i,Games.get(i).getName(),Games.get(i).getType(),Games.get(i).getMaxParticipants(),+Games.get(i).getGameStartHour(),Games.get(i).getGameStartMinutes(),Games.get(i).getGameStartDay(),Games.get(i).getGameStartMonth(),Games.get(i).getGameStartYear());
                            }
                            choice = input.nextInt();
                          }while(choice < 0 || choice > size);
                          GameName = Games.get(choice).getName();
                          if(Games.get(choice).getMaxParticipants()+1 == Options.ParticipantCount){     //After the user selected the game which he would like to add a participant,it then checks if the selected game has reached the maximum number of participants.
                              System.out.println("The selected game has reached the maximum number of participants!");
                          }else{
                              NewTournament.CreateParticipant(Participants,1,GameName);     //Creates a new participant.
                          }
                    }else if(choice==2){
                         NewTournament.EditParticipant(Participants);   //Edits a participant that is currently in the arraylist.
                    }else if(choice==3){
                        int size1 = Participants.size() - 1;
                        do{
                        System.out.println("\n");
                        System.out.println("Number\0(Name)\t(Nickname)\t(Type)\t(Communication)");
                        for (int i=0; i<Participants.size(); i++){
                        System.out.println(i+"\0"+ Participants.get(i).getPName()+"\t"+Participants.get(i).getNickname() +"\t"+Participants.get(i).getPType()+"\t"+Participants.get(i).getCommunication());               
                        }
                        System.out.println("Please type the participant you wish to remove(Type between Participant 0-"+size1+":");
                            choice = input.nextInt();
                            if(choice >= 0 && choice <=size1){
                            System.out.println(Participants.get(choice).getPName()+" has been deleted!\n");
                            Participants.remove(choice);    // //After printing the participants that are currently in the arraylist,it then removes the participant that the user chose.
                            }
                        }while(choice < 0 || choice > size1);
                        
                    }else if(choice==4){
                
                    }else{
                        System.out.println("Wrong input,please try again!\n");
                    }
                }while(choice==4);
        
            }else if(choice==3){
                do{
                    System.out.println("1. Draw Results:");
                    System.out.println("2. Add Results:");
                    System.out.println("3. Return\n");        
                    choice = input.nextInt();
            
                    if(choice ==1){
                        draw.draw(1, Games,Participants, number);   //The user chooses a game,then the draw begins...
                    }else if(choice==2){
                        draw.draw(2, Games,Participants, number);   //After the draw has ended,the user is able to input the results.
                    }else if(choice==3){
                
                    }else{
                        System.out.println("Wrong input,please try again!\n");
                    }                                            
                }while(choice!=3);
                
            }else if(choice==4){
                do{
                    System.out.println("1. Print the Draw:");       
                    System.out.println("2. Print the Results:");
                    System.out.println("3. Return\n");        
                    choice = input.nextInt();
                    
                    if(choice==1){
                        draw.draw(3, Games,Participants, number);   //Displays the draw.
                    }else if(choice==2){
                        draw.draw(4, Games,Participants, number);   //Displays the progress of the tournament.
                    }else if(choice==3){
                                        
                    }else{
                        System.out.println("Wrong input,please try again!\n");
                    }
                }while(choice!=3);
            }else if(choice==5){
                System.out.println("Bye Bye!");
            }else{
                System.out.println("Wrong input,please try again!\n");
            }
        }while(choice!=5);
    }
       
}
