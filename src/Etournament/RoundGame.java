
package Etournament;
/**
 *
 * @authors it21338,it21374
 */
public class RoundGame extends Game {    //The RoundGame class inherits all the fields and methods of class Game.
    private final int Wins = 3;     //States that the winner will be decided after 3 wins.

    public RoundGame(String Name, String Type, int MaxParticipants, int GameStartDay, int GameStartMonth, int GameStartYear, int GameStartHour, int GameStartMinutes, int GameStatus) {     //Constructor for RoundGame class.
        super(Name, Type, MaxParticipants, GameStartDay, GameStartMonth, GameStartYear, GameStartHour, GameStartMinutes, GameStatus);
    }
    
    
    
}
