
package Etournament;
/**
 *
 * @authors it21338,it21374
 */
public class Player extends Participant {        //The Player class inherits all the fields and methods of class Participant.
    private String Email;       //The player's email.

    public Player(String Email, String PName, String Nickname, int PType, String Communication, String Game) {  //Constructor of class Player.
        super(PName, Nickname, PType, Communication, Game);
        this.Email = Email;
    }

   

    public String getEmail() {      //Returns the player's email.
        return Email;
    }

    public void setEmail(String Email) {    //Sets the player's email.
        this.Email = Email;
    }
    
    
}
